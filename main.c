#include "functions.h"
int main(int argc, char *argv[])
{
    char username[128];
    char ip[128];
    int port;

    if(argc == 3)
    {
        if(strcmp(argv[1], "server") == 0)
        {
            port = atoi(argv[2]);
            Server(port);
        }
        else
        {
            help_message();
        }
    }
    else if(argc == 5)
    {
        if(strcmp(argv[1], "client") == 0)
        {
            strcpy(username, argv[2]);
            strcpy(ip, argv[3]);
            port = atoi(argv[4]);
            Client(username, ip, port);
        }
        else
        {
            help_message();
        }
    }
    else
    {
        help_message();
    }

    return 0;
}
void help_message()
{
    fprintf(stderr, "Usage:\t./chat client username ip port\n");
    fprintf(stderr, "Or:\t./chat server port\n");
    exit(1);
}
