#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <strings.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/mman.h>

#define LISTENQ 10
#define MAX_CLIENTS 10
#define BUFLEN  512


typedef struct str_client_list
{
    int socket;
    char username[32];
    char ip[32];
} CList;


//Server
void Server(int port);

void error_gen(const char* );
void listen_socket(int *listen_sd, struct sockaddr_in *server, int port);
int NewClient(int pos, int sock, char *name, char *addr);
int removeClient(int sock);
void print_clients();
void catch_int(int signo);

//Client
void Client(char* username, char* ip, int port);

//Main
void help_message();

#endif
