#include "functions.h"
extern CList Clients[MAX_CLIENTS];
extern int CountClients;
void error_gen(const char* message)
{
    perror (message);
    exit (EXIT_FAILURE);
}
/* create a listening socket for server */
void listen_socket(int *listen_sd, struct sockaddr_in *server, int port)
{
    // Create a stream socket
    if ((*listen_sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        error_gen("Cannot Create Socket!");
    // Bind an address to the socket
    memset(server, 0, sizeof(struct sockaddr_in));
    server->sin_family = AF_INET;
    server->sin_port = htons(port);
    server->sin_addr.s_addr = htonl(INADDR_ANY); // Accept connections from any client
}
// adds a client to list by setting username and socket

int NewClient(int pos, int sock, char *name, char *addr)
{
    Clients[pos].socket = sock;
    strcpy(Clients[pos].username, name);
    strcpy(Clients[pos].ip, addr);
    CountClients++;
    print_clients();
    return 0;
}
/* remove a client from list by resetting socket  */
int removeClient(int sock)
{
    int k;
    for(k = 0; k < MAX_CLIENTS; k++)
    {
        /* checks for specific client */
        if(Clients[k].socket == sock)
        {
            printf("%s has disconnected \n", Clients[k].username);
            /* reset client */
            memset(Clients[k].username, 0, sizeof(Clients[k].username));
            memset(Clients[k].ip, 0, sizeof(Clients[k].ip));

            Clients[k].socket = -1;
        }
    }
    CountClients--;
    print_clients();
    return 0;
}
/* prints list of clients on server side */
void print_clients()
{
    int k;
    printf("====================================\n");
    for(k = 0; k < MAX_CLIENTS; k++)
    {
        if(Clients[k].socket > -1)
        {
            printf("- %s\n", Clients[k].username);
        }
    }
    printf("%d clients are on the chat \n", CountClients);
    printf("====================================\n\n");
}

