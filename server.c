#include "functions.h"
CList Clients[MAX_CLIENTS];
int CountClients;
int client[FD_SETSIZE];
void Server(int port)
{
    char serverName[BUFLEN];
    printf("Please enter a server name: \n");
    fgets(serverName, BUFLEN, stdin);
    int  listen_sd, new_sd, sockfd, maxfd,maxi, bytes_to_read, i, j, k, nready, lengthCN;
    char sbuf[BUFLEN],*bp,buf[BUFLEN],ctrlCharacter1,ctrlCharacter2,ctrlCharacter3, clientName[BUFLEN];
    char* msgUser;
    socklen_t client_len;
    struct sockaddr_in server, client_addr;
    int n;
    fd_set rset, allset;
    printf("Server name: %s", serverName);
    printf("Server listening on port: %d\n", port);
    listen_socket(&listen_sd, &server, port);
    if (bind(listen_sd, (struct sockaddr *)&server, sizeof(server)) == -1)
        error_gen("bind error");
    listen(listen_sd, LISTENQ);
    maxfd = listen_sd;
    maxi = -1;
    CountClients = 0;
    for(i = 0; i < FD_SETSIZE; i++)
        client[i] = -1;
    FD_ZERO(&allset);
    FD_SET(listen_sd, &allset);
    /* set sockets in client struct to -1 */
    for(i = 0; i < MAX_CLIENTS; i++)
        Clients[i].socket = -1;
    while(1)
    {
        memset(buf, 0, BUFLEN);
        rset = allset;               // structure assignment
        nready = select(1024, &rset, NULL, NULL, NULL);
        if (FD_ISSET(listen_sd, &rset)) // new client connection
        {
            client_len = sizeof(client_addr);
            if ((new_sd = accept(listen_sd, (struct sockaddr *) &client_addr, &client_len)) == -1)
                error_gen("accept error");
            bp = buf;
            read(new_sd, bp, BUFLEN);
            sscanf(bp, "%c %c %d %s %c", &ctrlCharacter1, &ctrlCharacter2, &lengthCN, clientName, &ctrlCharacter3);
            for (i = 0; i < FD_SETSIZE; i++)
            {
                //if it is a new client 
                if (client[i] < 0)
                {
                    client[i] = new_sd;    // save descriptor
                    printf("%s has connected \n", clientName);
                    // send server name
                    memset(sbuf, 0, BUFLEN);
                    sprintf(sbuf, "%c %c %zu %s %c", 0x16, 0x03, strlen(serverName), serverName, 0x04);
                    write(client[i], sbuf, BUFLEN);
                    for(j = 0; j < MAX_CLIENTS; j++)
                    {
                        if(Clients[j].socket == -1)
                        {
                            if(CountClients == 0)
                                NewClient(j, i, clientName, inet_ntoa(client_addr.sin_addr));
                            else
                                NewClient(j, i, clientName, inet_ntoa(client_addr.sin_addr));
                            break;
                        }
                    }
                    break;
                }
            }
            if (i == FD_SETSIZE)
            {
                printf ("Too many clients\n");
                exit(1);
            }
            memset(sbuf, 0, BUFLEN);
            strcat(sbuf, inet_ntoa(client_addr.sin_addr));
            strcat(sbuf, " ");
            strcat(sbuf, clientName);
            strcat(sbuf, " joined the chat channel.\n");
            /* notify other users that a new user has joined the chat */
            for(j = 0; j <= maxfd; j++)
            {
                if(client[j] < 0)
                    continue;
                if(client[j] == new_sd)
                    continue;
                write(client[j], sbuf, BUFLEN);
            }
            FD_SET (new_sd, &allset);     // add new descriptor to set
            if (new_sd > maxfd)
                maxfd = new_sd;    // for select
            if (i > maxi)
                maxi = i;    // new max index in client[] array
            if (--nready <= 0)
                continue;    // no more readable descriptors
        }
        for (i = 0; i <= maxi; i++)    // check all clients for data
        {
            if ((sockfd = client[i]) < 0)
                continue;
            if (FD_ISSET(sockfd, &rset))
            {
                bp = buf;
                bytes_to_read = BUFLEN;
                while ((n = read(sockfd, bp, bytes_to_read)) > 0)
                {
                    bp += n;
                    bytes_to_read -= n;
                }
                /* displays list of current users and their status */
                if(strstr(buf, "/users"))
                {
                    for(k = 0; k < MAX_CLIENTS; k++)
                    {
                        if(Clients[k].socket > -1)
                        {
                            for(j = 0; j <= maxfd; j++)
                            {
                                if(j == i)
                                {
                                    sprintf(buf, "%c", '-');
                                    strcat(buf, " ");
                                    strcat(buf, Clients[k].username);
                                    strcat(buf,"\n");
                                    write(client[j], buf, BUFLEN);
                                }
                            }
                        }
                    }
                    sprintf(buf, "%d", CountClients);
                    strcat(buf, " users in chat.\n\n");
                    write(client[i], buf, BUFLEN);
                    break;
                }
                if(strstr(buf, "/msg"))
                {        
                        /* get the name of the reciever with strtok*/
                        int len = strlen(buf);
                        char * fullName = (char *) malloc( len );
                        strcpy( fullName, buf);
                            char d[] = " ";
                            char *p = strtok(fullName, d);
                            p= strtok(NULL, d);
                            p= strtok(NULL, d);   
                    for(k = 0; k < MAX_CLIENTS; k++)
                    {   
                        if((Clients[k].socket > -1) && !strcmp(Clients[k].username,p))
                        {
                            for(j = 0; j <= maxfd; j++)
                            {
                                if(j == Clients[k].socket){
                                    write(client[j], buf, BUFLEN);
                                }
                            }
                        }
                    }
                    break;
                }
                /* disconnected signal */
                if(strstr(buf, "has left the chat"))
                {
                    for(j = 0; j <= maxfd; j++)
                    {
                        if(j == i)
                        {
                            removeClient(j);
                            client[i] = -1;
                        }
                        else
                            write(client[j], buf, BUFLEN);
                    }
                    break;
                }
                /* if users enters invalid command */
                if(strstr(buf, "/"))
                {
                    write(client[i], "type /help for a list of commands\n", BUFLEN);
                }else
                {
                    for(j = 0; j <= maxfd; j++)
                    {
                        if(j != i)
                            write(client[j], buf, BUFLEN);
                    }
                }
                if (--nready <= 0)
                    break;        // no more readable descriptors
            }
        }
    }
    close(sockfd);
    exit(0);
}

