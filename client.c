#include "functions.h"
int GLOBAL;
char* user ;
void Client(char* username, char* host, int port)
{
    int     n,bytes_to_read,sd,lengthSN;
    char   *bp,rbuf[BUFLEN],sbuf[BUFLEN], command[BUFLEN],serverName[BUFLEN],ctrlChar1,ctrlChar2, ctrlChar3;
    struct hostent	*hp;
    struct sockaddr_in server;
    user = username;
    /*
    Un signal est un message émit à destination d'un processus 
    pour l'informer que quelque chose vient de survenir ou pour 
    lui demander de réaliser une tâche particulière. 
    La fonction signal permet de définir le gestionnaire de signal à invoquer en cas de réception 
    d'un signal particulier. 
    SIGNINT =Signal Interrupt
    SOURCE : https://koor.fr/C/csignal/signal.wp
    */
    signal(SIGINT, catch_int);
    // Create the socket
    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Cannot create socket");
        exit(1);
    }
    bzero((char *)&server, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    if ((hp = gethostbyname(host)) == NULL)
    {
        fprintf(stderr, "Unknown server address\n");
        exit(1);
    }
    bcopy(hp->h_addr, (char *)&server.sin_addr, hp->h_length);

    // Connecting to the server
    if (connect (sd, (struct sockaddr *)&server, sizeof(server)) == -1)
    {
        fprintf(stderr, "Can't connect to server\n");
        perror("connect");
        exit(1);
    }
    GLOBAL = sd;
    sbuf[0] = '\0';
    sprintf(sbuf, "%c %c %zu %s %c", 0x16, 0x02, strlen(username), username, 0x04);
    send(sd, sbuf, BUFLEN, 0);
    // read the server name
    memset(rbuf, 0, BUFLEN);
    read(sd, rbuf, BUFLEN);
    sscanf(rbuf, "%c %c %d %s %c", &ctrlChar1, &ctrlChar2, &lengthSN, serverName, &ctrlChar3);
    printf("Connected Server Name: %s\n", serverName);
    switch(fork())
    {
    case -1:
        perror("fork call");
        exit(1);
    case 0:
        while(1)
        {
            bp = rbuf;
            bytes_to_read = BUFLEN;
            n = 0;
            while((n = recv(sd, bp, bytes_to_read, 0)) < BUFLEN)
            {
                bp += n;
                bytes_to_read -= n;
            }
            printf("%s", rbuf);
        }
    default:
        while(1)
        {
            memset(sbuf, 0, BUFLEN);
            memset(command, 0, BUFLEN);
            fgets (command, BUFLEN, stdin);
            strcpy(sbuf, username);
            /* displays list of commands */
            if(!strcmp(command, "/help\n"))
            {
                printf("==========================================================\n");
                printf("/users => displays list of current users and status\n");
                printf("/msg <username> <text> => send a message to a specific user\n");
                printf("/quit => leave the chat\n\n");
                printf("\n");
            }
            /* user quits the application */
            if(!strcmp(command, "/quit\n"))
            {
                strcat(sbuf, " has left the chat\n");
                send (sd, sbuf, BUFLEN, 0);
                kill(0, SIGINT);
            }
            /* toggles logging chat */
                strcat(sbuf, ": ");
                strcat(sbuf, command);
                send (sd, sbuf, BUFLEN, 0);
        }
    }
    close(sd);
    return;
}
/* kill process upon termination */
void catch_int(int signo)
{
    if(signo == SIGINT)
    {
        char sbuf[BUFLEN];
        strcpy(sbuf, user);
        strcat(sbuf, " has left the chat\n");
        send (GLOBAL, sbuf, BUFLEN, 0);
        close(GLOBAL);
        //Signal Terminate
        kill(getpid(), SIGTERM);
    }
}
