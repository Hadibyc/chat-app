CC=gcc
OUTPUTFILENAME=chat

main: main.o client.o server.o help_functions.o
	$(CC) $(LFLAGS) main.o client.o server.o help_functions.o -o $(OUTPUTFILENAME)

clean:
	rm -f *.o core.* $(OUTPUTFILENAME)

main.o: main.c
	$(CC) $(FLAGS) -c main.c

client.o: client.c
	$(CC) $(FLAGS) -c client.c

server.o: server.c
	$(CC) $(FLAGS) -c server.c
	
help_functions.o: help_functions.c
	$(CC) $(FLAGS) -c help_functions.c
